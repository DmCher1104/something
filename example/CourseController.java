package example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class CourseController {

    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    public void addCourse(CourseObj courseObj) {
        courseService.addCourse(courseObj);
    }

    public void removeCourse(Integer id) {
        courseService.removeCourse(id);
    }

    public CourseObj getById(Integer id) {
        CourseObj byId = courseService.getById(id);
        return byId;
    }

    public ArrayList<CourseObj> getAll () {
        ArrayList<CourseObj> all = courseService.getAll();
        return all;
    }

    public void update(CourseObj courseOld, CourseObj courseNew) {
        courseService.update(courseOld, courseNew);
    }
}
