package example;

import java.util.Objects;

public class CourseObj {
    private static Integer startId = 0;
    private Integer id = startId;
    private String CourseName;
    private String program;
    private Integer hours;

    public CourseObj(String courseName, String program, Integer hours) {
        this.id = ++startId;
        CourseName = courseName;
        this.program = program;
        this.hours = hours;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseObj courseObj = (CourseObj) o;
        return Objects.equals(id, courseObj.id) &&
                Objects.equals(CourseName, courseObj.CourseName) &&
                Objects.equals(program, courseObj.program) &&
                Objects.equals(hours, courseObj.hours);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, CourseName, program, hours);
    }

    @Override
    public String toString() {
        return "CourseObj{" +
                "id=" + id +
                ", CourseName='" + CourseName + '\'' +
                ", program='" + program + '\'' +
                ", hours=" + hours +
                '}';
    }
}
