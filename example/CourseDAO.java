package example;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;

@Component
public class CourseDAO {
    private static ArrayList<CourseObj> arrayList;
     static {
        arrayList = new ArrayList<>();
     }

    public void addCourse(CourseObj courseObj) {
         arrayList.add(courseObj);
    }

    public void removeCourse(Integer id) {
         Iterator<CourseObj> arr = arrayList.iterator();
         while (arr.hasNext()) {
             CourseObj courseObj = arr.next();
             if (courseObj.getId().equals(id)) {
                 arr.remove();
             }
         }
    }

    public CourseObj getById(Integer id) {
        for (CourseObj a : arrayList) {
            if (a.getId().equals(id)) {
                return a;
            }
        }
        return null;
    }

    public ArrayList<CourseObj> getAll () {
         return arrayList;
    }

    public void update(CourseObj courseOld, CourseObj courseNew) {
         for (CourseObj a: arrayList) {
             if (a.equals(courseOld)) {
                 a.setCourseName(courseNew.getCourseName());
                 a.setProgram(courseNew.getProgram());
                 a.setHours(courseNew.getHours());
             }
         }
    }
}
