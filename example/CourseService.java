package example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class CourseService {

    private final CourseDAO courseDAO;

    @Autowired
    public CourseService(CourseDAO courseDAO) {
        this.courseDAO = courseDAO;
    }

    public void addCourse(CourseObj courseObj) {
        courseDAO.addCourse(courseObj);
    }

    public void removeCourse(Integer id) {
        courseDAO.removeCourse(id);
    }

    public CourseObj getById(Integer id) {
        CourseObj byId = courseDAO.getById(id);
        return byId;
    }

    public ArrayList<CourseObj> getAll () {
        ArrayList<CourseObj> all = courseDAO.getAll();
        return all;
    }

    public void update(CourseObj courseOld, CourseObj courseNew) {
        courseDAO.update(courseOld, courseNew);
    }
}
