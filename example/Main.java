package example;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Factory.class);

        CourseController controller = applicationContext.getBean("courseController", CourseController.class);
        CourseObj course1 = new CourseObj("Java", "SE", 40);
        CourseObj course2 = new CourseObj("Python", "SE", 20);
        CourseObj course3 = new CourseObj("C++", "SE", 100);
        CourseObj course4 = new CourseObj("C#", "SE", 200);
        CourseObj course5 = new CourseObj("Scala", "SE", 500);

        controller.addCourse(course1);
        controller.addCourse(course2);
        controller.addCourse(course3);
        controller.addCourse(course4);

        controller.removeCourse(1);

        CourseObj byId = controller.getById(2);
        System.out.println(byId + "\n");

        controller.update(course3, course5);

        ArrayList<CourseObj> all = controller.getAll();
        for (CourseObj a : all) {
            System.out.println(a);
        }
    }
}
