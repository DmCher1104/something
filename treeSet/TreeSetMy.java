package treeSet;

public class TreeSetMy<E> implements Tree<E> {
    private Leaf<E> root;
    private int size = 0;

    public TreeSetMy() {
        root = new Leaf<>(null);
    }

    public boolean add(E e) {
        if (size == 0) {
            return initRootLeaf(e);
        }
        Leaf<E> newLeaf = new Leaf<>(e);
        Leaf<E> lastLeaf = findLastLeaf(root, newLeaf);

        if(lastLeaf == null) {
            return false;
        }

        size++;
        newLeaf.parent = lastLeaf;

        if (lastLeaf.compareTo(newLeaf) > 0) {
            lastLeaf.right = newLeaf;
            return true;
        } else {
            lastLeaf.left = newLeaf;
            return true;
        }

    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E get(E e) {
        Leaf<E> eLeaf = new Leaf<>(e);
        return binarySearch(root, eLeaf);
    }

    private E binarySearch(Leaf<E> leaf, Leaf<E> eLeaf) {
        int compare = leaf.compareTo(eLeaf);

        if (compare > 0 && leaf.right != null)
            return binarySearch(leaf.right, eLeaf);

        if (compare < 0 && leaf.left != null)
            return binarySearch(leaf.left, eLeaf);

        if (compare == 0)
            return leaf.getElement();

        return null;
    }


    private boolean initRootLeaf(E e) {
        root.element = e;
        size++;
        return true;
    }

    private Leaf<E> findLastLeaf(Leaf<E> oldLeaf, Leaf<E> newLeaf) {
        Leaf<E> lastLeaf = oldLeaf;
        int compare = oldLeaf.compareTo(newLeaf);

        if(compare > 0 && oldLeaf.right != null) {
            lastLeaf = findLastLeaf(oldLeaf.right, newLeaf);
            return lastLeaf;
        }

        if(compare < 0 && oldLeaf.left != null) {
            lastLeaf = findLastLeaf(oldLeaf.left, newLeaf);
            return lastLeaf;
        }

        if (compare == 0) {
            return null;
        }
        return lastLeaf;
    }


    class Leaf<E> implements Comparable<E> {
        private Leaf<E> parent;
        private Leaf<E> right;
        private Leaf<E> left;
        private E element;

        public Leaf(E element) {
            this.element = element;
        }

        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }


        @Override
        public int compareTo(Object o) {
            Leaf<E> node = (Leaf<E>) o;
            return node.hashCode() - this.hashCode();
        }

        @Override
        public int hashCode() {
            int hash = 31;
            hash = hash * 17 + element.hashCode();
            return hash;
        }
    }
}
