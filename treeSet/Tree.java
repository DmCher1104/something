package treeSet;

public interface Tree<E> {
    boolean add(E e);
    int size();
    E get(E e);
}
