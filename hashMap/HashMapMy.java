package hashMap;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class HashMapMy<K, V> {
    private int size = 16;
    private final List<LinkedList<Entry<K, V>>> mapList;

    public HashMapMy() {
        mapList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            mapList.add(new LinkedList<Entry<K, V>>());
        }
    }

    public void put(K key, V value) {
        int index = index(key);
        LinkedList<Entry<K, V>> bucket = mapList.get(index);
        Entry<K, V> entryToPut = new Entry<>(key, value);
        if (bucket.contains(entryToPut)) {
            bucket.remove(entryToPut);
            bucket.add(entryToPut);
        } else {
            bucket.add(entryToPut);
        }
    }

    public V get(K key) {
        int index = index(key);
        LinkedList<Entry<K, V>> bucket = mapList.get(index);
        for (Entry<K, V> entry : bucket) {
            if (entry.getKey().equals(key)) {
                return entry.getValue();
            }
        }
        return null;
    }

    public int size () {
        return size;
    }

    public boolean containsValue(V value) {
        for (int i = 0; i < size; i++) {
            LinkedList<Entry<K, V>> bucket = mapList.get(i);
            for (Entry<K, V> entry : bucket) {
                if (entry.getValue().equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containsKey(K key) {
        int index = index(key);
        LinkedList<Entry<K, V>> bucket = mapList.get(index);
        for (Entry<K, V> entry : bucket) {
            if (entry.getKey().equals(key)){
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty() {
        return size == 0;
    }


    public ArrayList<V> values() {
        ArrayList<V> tmp = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            LinkedList<Entry<K, V>> bucket = mapList.get(i);
            for (Entry<K, V> entry : bucket) {
                tmp.add(entry.getValue());
            }
        }
        return tmp;
    }

    public void remove(K key) {
        int index = index(key);
        LinkedList<Entry<K, V>> bucket = mapList.get(index);
        for (Entry<K, V> entry : bucket) {
            if (entry.getKey().equals(key)){
                bucket.remove(entry);
            }
        }
    }

    public int index (K key) {
        int hashcode = key.hashCode();
        int index = hashcode % size;
        return index;
    }

}
