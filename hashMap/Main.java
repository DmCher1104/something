package hashMap;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        HashMapMy hashMapMy = new HashMapMy();
        hashMapMy.put("Макс", 30);
        hashMapMy.put("Антон", 40);
        hashMapMy.put("Макс", 50);
        hashMapMy.put("Маша", 60);
        hashMapMy.put("Антон", 10);


        System.out.println(hashMapMy.get("c"));
        System.out.println(hashMapMy.get("Макс"));

        System.out.println(hashMapMy.size());

        System.out.println(hashMapMy.containsValue(10));
        System.out.println(hashMapMy.containsValue(2));

        System.out.println(hashMapMy.containsKey("Антон"));
        System.out.println(hashMapMy.containsKey("Максим"));

        ArrayList values = hashMapMy.values();
        for (Object value : values) {
            System.out.println(value);
        }

        hashMapMy.remove("Антон");

    }
}
